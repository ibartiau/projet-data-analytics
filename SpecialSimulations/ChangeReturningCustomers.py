#Question 3: What would happen if we lower the returning customers to 50 and simulate the same period?
from Code.Simulations import generate_coffeebar
from SpecialSimulations.Functions import probabilityTypeCustomer, nbReturners
import matplotlib.pyplot as plt

# Create a dictonary with prices
prices = {"milkshake": 5, "frappucino": 4, "water": 2, "soda": 3, "coffee": 3, "tea": 3,
          "sandwich": 5, "cookie": 2, "pie": 3, "muffin": 3, "nothing": 0}
df_coffeebar_simulation, tracks_returners, all_returners = generate_coffeebar(1000, prices, 250, 500, 1)
df_coffeebar_sim_returners, tracks_returners, all_returners = generate_coffeebar(50, prices, 250, 500, 1)


#Comparaison of the probability (in %) of having a returner for each hour, if there are 1000 or 50 returners.
#Calculations:

times = df_coffeebar_simulation["TIME"].str[11:13].unique()
proba_hour1 = {}
proba_hour2 = {}
count = {}

for time in times:
    proba_hour1[time] = 0
    proba_hour2[time] = 0
    count[time] = 0
#Calculate the probability of having a returner for each time, and collect the result in a dictionary for each hour (08, 09,...)
id_returnings = df_coffeebar_simulation.set_index('CUSTOMER').index.get_duplicates()
nb_returners, nb_total = nbReturners(df_coffeebar_simulation, id_returnings)
for index, hour in enumerate(df_coffeebar_simulation["TIME"].str[11:16].unique()):
    count[hour[0:2]] += 1
    proba = probabilityTypeCustomer(hour, "returner", nb_returners, nb_total)
    proba_hour1[hour[0:2]] += proba

id_returnings = df_coffeebar_sim_returners.set_index('CUSTOMER').index.get_duplicates()
nb_returners, nb_total = nbReturners(df_coffeebar_sim_returners, id_returnings)
for index, hour in enumerate(df_coffeebar_sim_returners["TIME"].str[11:16].unique()):
    proba = probabilityTypeCustomer(hour, "returner", nb_returners, nb_total)
    proba_hour2[hour[0:2]] += proba

#Divide the values to have the mean of the probability for each hour.
for i in proba_hour1:
    proba_hour1[i] /= count[i]
for i in proba_hour2:
    proba_hour2[i] /= count[i]

#Plot:
barWidth = 0.4
y1 = proba_hour1
y2 = proba_hour2
r1 = range(len(y1))
r2 = [x + barWidth for x in r1]

g1 = plt.bar(r1, y1.values(), width = barWidth, color = ['yellow' for i in y1],
           edgecolor = ['blue' for i in y1])
g2 = plt.bar(r2, y2.values(), width = barWidth, color = ['blue' for i in y1],
           edgecolor = ['green' for i in y1])
plt.legend([g1, g2], ['Proba (in %) returner if total returner = 1000', 'Proba (in %) returner if total returner = 50'],loc = 'upper right')
plt.xticks([r + barWidth for r in range(len(y1))], times)