import pandas as pd
from Code.Simulations import generate_coffeebar
import matplotlib.pyplot as plt
#We want to see the impact if the probabilitiy of having a tripadvisor goes up to 40%

coffeebar = pd.read_csv("./Data/Coffeebar_2013-2017.csv", sep=";")

prices = {"milkshake": 5, "frappucino": 4, "water": 2, "soda": 3, "coffee": 3, "tea": 3,
          "sandwich": 5, "cookie": 2, "pie": 3, "muffin": 3, "nothing": 0}

df_coffeebar_simulation, tracks_returners, all_returners = generate_coffeebar(1000, prices, 250, 500, 1)
df_coffeebar_simulation_new, tracks_returners, all_returners = generate_coffeebar(1000, prices, 250, 500, 4)


#Average income in terms of hour
times = df_coffeebar_simulation["TIME"].str[11:13].unique()
total_amount = {}
total_amount_new = {}
for time in times:
    total_amount[time] = 0
    total_amount_new[time] = 0

for index, hour in enumerate(df_coffeebar_simulation["TIME"]):
    total_amount[hour[11:13]] += df_coffeebar_simulation["AMOUNT_PAID"].iloc[index]
for index, hour in enumerate(df_coffeebar_simulation_new["TIME"]):
    total_amount_new[hour[11:13]] += df_coffeebar_simulation_new["AMOUNT_PAID"].iloc[index]

#Plot
barWidth = 0.4

y1 = total_amount
y2 = total_amount_new
r1 = range(len(y1))
r2 = [x + barWidth for x in r1]

g1 = plt.bar(r1, y1.values(), width = barWidth, color = ['yellow' for i in y1],
           edgecolor = ['blue' for i in y1])
g2 = plt.bar(r2, y2.values(), width = barWidth, color = ['blue' for i in y1],
           edgecolor = ['green' for i in y1])
plt.legend([g1, g2], ['Probability of having a tripadvisor = 10%', 'Probability of having a tripadvisor = 40%'],loc = 'upper right')
plt.xticks([r + barWidth for r in range(len(y1))], total_amount.keys())