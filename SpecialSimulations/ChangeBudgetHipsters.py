#Question 5

from Code.Simulations import generate_coffeebar
from SpecialSimulations.CountReturningCustomers import probabilityTypeCustomer
# Create a dictonary with prices
prices = {"milkshake": 5, "frappucino": 4, "water": 2, "soda": 3, "coffee": 3, "tea": 3,
          "sandwich": 5, "cookie": 2, "pie": 3, "muffin": 3, "nothing": 0}

df_coffeebar_simulation, tracks_returners, all_returners = generate_coffeebar(50, prices, 250, 500, 1)
print(df_coffeebar_simulation['SUB_TYPE_CUSTOMER'].value_counts())

df_coffeebar_sim_hipster, tracks_returners, all_returners = generate_coffeebar(50, prices, 250, 40, 1)
print(df_coffeebar_sim_hipster['SUB_TYPE_CUSTOMER'].value_counts())