from Code.Simulations import generate_coffeebar
import matplotlib.pyplot as plt
import pandas as pd

prices = {"milkshake": 5, "frappucino": 4, "water": 2, "soda": 3, "coffee": 3, "tea": 3,
          "sandwich": 5, "cookie": 2, "pie": 3, "muffin": 3, "nothing": 0}
df_coffeebar_simulation, tracks_returners, all_returners = generate_coffeebar(1000, prices, 250, 500, 1)

for i in prices:
    prices[i] *= 1.2

df_coffeebar_new_prices, tracks_returners, all_returners = generate_coffeebar(1000, prices, 250, 500, 1)

frames = [df_coffeebar_simulation[:124830], df_coffeebar_new_prices[124830::]]
#with this way of working, we'll have a problem with identical customerID but it is not important here

df_coffeebar_simulation_prices = pd.concat(frames)

#Average income with the hour changed.
times = df_coffeebar_simulation_prices["TIME"].str[11:13].unique()
total_amount1 = {}
total_amount2 = {}

for time in times:
    total_amount1[time] = 0
    total_amount2[time] = 0

for index, hour in enumerate(df_coffeebar_simulation["TIME"]):
    total_amount1[hour[11:13]] += df_coffeebar_simulation["AMOUNT_PAID"].iloc[index]
for index, hour in enumerate(df_coffeebar_simulation_prices["TIME"]):
    total_amount2[hour[11:13]] += df_coffeebar_simulation_prices["AMOUNT_PAID"].iloc[index]

#Comparison of the revenue per hour before and after the increases of prices.
barWidth = 0.4

y1 = total_amount1
y2 = total_amount2
r1 = range(len(y1))
r2 = [x + barWidth for x in r1]

g1 = plt.bar(r1, y1.values(), width = barWidth, color = ['yellow' for i in y1],
           edgecolor = ['blue' for i in y1])
g2 = plt.bar(r2, y2.values(), width = barWidth, color = ['blue' for i in y1],
           edgecolor = ['green' for i in y1])
plt.legend([g1, g2], ['Normal prices', 'With the increases of prices'],loc = 'upper right')
plt.xticks([r + barWidth for r in range(len(y1))], total_amount1.keys())