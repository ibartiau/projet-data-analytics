import pandas as pd
from Code.Simulations import generate_coffeebar
from Code.ClassesOfCustomers import Customer, OneTimeCustomer, ReturningCustomer
import random
coffeebar = pd.read_csv("./Data/Coffeebar_2013-2017.csv", sep=";")

prices = {"milkshake": 5, "frappucino": 4, "water": 2, "soda": 3, "coffee": 3, "tea": 3,
          "sandwich": 5, "cookie": 2, "pie": 3, "muffin": 3, "nothing": 0}

df_coffeebar_simulation, tracks_returners, all_returners = generate_coffeebar(1000, prices, 250, 500, 1)

all_returners_customerid = []
for i in range(len(all_returners)):
    all_returners_customerid.append(all_returners[i][0])

for j in range (5):
    customerid = random.choice(all_returners_customerid)
    print(tracks_returners[customerid])