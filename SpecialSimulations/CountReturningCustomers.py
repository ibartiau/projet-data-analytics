from Code.Simulations import generate_coffeebar
from Code.Exploratory import coffeebar
from SpecialSimulations.Functions import nbReturners, probabilityTypeCustomer
prices = {"milkshake": 5, "frappucino": 4, "water": 2, "soda": 3, "coffee": 3, "tea": 3,
          "sandwich": 5, "cookie": 2, "pie": 3, "muffin": 3, "nothing": 0}

df_coffeebar_simulation, tracks_returners, all_returners = generate_coffeebar(1000, prices, 250, 500, 1)

# Question 2
#The functions used are is the file: "Functions".

#Count returning customers in the original data.
id_returnings = coffeebar.set_index('CUSTOMER').index.get_duplicates()
print (len(id_returnings))

#Count the number of returners for each time.
nb_returners, nb_total = nbReturners(coffeebar,id_returnings)
print (nb_returners)

#Determine the probability of having a one time or returning customer at any given time.
#Example:
proba_one_timer = probabilityTypeCustomer("08:00", "one timer", nb_returners, nb_total)
print(proba_one_timer)

