
#count returning customers in the original data.
def nbReturners(coffeebar, id_returnings):
    times = coffeebar["TIME"].str[11:16].unique()
    nb_returners = {}
    nb_total = {}
    for time in times:
        nb_returners[time] = 0
        nb_total[time] = 0
    for index, hour in enumerate(coffeebar["TIME"]):
        nb_total[hour[11:16]] += 1
        if coffeebar["CUSTOMER"].iloc[index] in id_returnings:
            nb_returners[hour[11:16]] += 1

    return nb_returners, nb_total

#Determine the probability of having a one time or returning customer at any given time.
def probabilityTypeCustomer(hour, type, nb_returners, nb_total):
    #form of hour: 08:00. Hour must be in times.
    proba = (nb_returners[hour]/nb_total[hour])*100
    if type == "returner":
        return proba
    elif type == "one timer":
        return 100-proba
    else:
        return "Wrong type."

