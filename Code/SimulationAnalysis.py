import pandas as pd
from Code.Simulations import generate_coffeebar
coffeebar = pd.read_csv("./Data/Coffeebar_2013-2017.csv", sep=";")
coffeebar.fillna("nothing", inplace=True)

# Create a dictonary with prices
prices = {"milkshake": 5, "frappucino": 4, "water": 2, "soda": 3, "coffee": 3, "tea": 3,
          "sandwich": 5, "cookie": 2, "pie": 3, "muffin": 3, "nothing": 0}

df_coffeebar_simulation, tracks_returners, all_returners = generate_coffeebar(1000, prices, 250, 500, 1)

############################################################################
#Compare new and old data, with the number of food or drinks sold:
#Simulation
foods_sim_count = df_coffeebar_simulation["FOOD"].value_counts()
drinks_sim_count = df_coffeebar_simulation["DRINKS"].value_counts()
#Original data
foods_count = coffeebar["FOOD"].value_counts()
drinks_count = coffeebar["DRINKS"].value_counts()


#Plot drinks:
import matplotlib.pyplot as plt
barWidth = 0.4

r1 = range(len(drinks_count))
r2 = [x + barWidth for x in r1]

g1 = plt.bar(r1, drinks_count, width = barWidth, color = ['yellow' for i in drinks_count],edgecolor = ['blue' for i in drinks_count])
g2 = plt.bar(r2, drinks_sim_count, width = barWidth, color = ['red' for i in drinks_count],edgecolor = ['green' for i in drinks_count])
plt.legend([g1, g2], ['coffeebar', 'coffeebar simulation'],loc = 'upper right')
plt.xticks([r + barWidth for r in range(len(drinks_count))], coffeebar["DRINKS"].unique())

#Plot foods:
barWidth = 0.4
r1 = range(len(foods_count))
r2 = [x + barWidth for x in r1]

g1 = plt.bar(r1, foods_count, width = barWidth, color = ['yellow' for i in foods_count],edgecolor = ['blue' for i in foods_count])
g2 = plt.bar(r2, foods_sim_count, width = barWidth, color = ['red' for i in foods_count],edgecolor = ['green' for i in foods_count])
plt.legend([g1, g2], ['coffeebar', 'coffeebar simulation'],loc = 'upper right')
plt.xticks([r + barWidth for r in range(len(foods_count))], coffeebar["FOOD"].unique())

############################################################################
#Average income in the day (for the simulation, then the original data)
#Simulation:
total_sold_sim = 0
for amount in df_coffeebar_simulation["AMOUNT_PAID"]:
    total_sold_sim += amount
mean_amount_sim = total_sold_sim / len(df_coffeebar_simulation["TIME"])

#Original data:
total_sold = 0
for drink in coffeebar["DRINKS"]:
    if drink == "milkshake":
        total_sold += 5
    elif drink == "frapuccino":
        total_sold += 4
    elif drink == "water":
        total_sold += 2
    else:
        total_sold += 3

for food in coffeebar["FOOD"]:
    if food == "sandwich":
        total_sold += 5
    elif food == "cookie":
        total_sold += 2
    elif food == "pie":
        total_sold += 3
    elif food == "muffin":
        total_sold += 3
mean_amount = total_sold / len(coffeebar["TIME"])

diff = mean_amount - mean_amount_sim
print ("The difference between the two average amounts is : ", diff)

############################################################################
#Average income in terms of hour
times = df_coffeebar_simulation["TIME"].str[11:13].unique()
total_amount = {}
for time in times:
    total_amount[time] = 0

for index, hour in enumerate(df_coffeebar_simulation["TIME"]):
    total_amount[hour[11:13]] += df_coffeebar_simulation["AMOUNT_PAID"].iloc[index]

#Plot
plt.bar(total_amount.keys(), total_amount.values())

