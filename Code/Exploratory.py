import pandas as pd
coffeebar = pd.read_csv("./Data/Coffeebar_2013-2017.csv", sep=";")

#Ex 1 : what drinks are sold in the bar?
print(coffeebar["DRINKS"].unique())

#what food are sold in the bar?
print(coffeebar["FOOD"].dropna().unique())

#How many customers did the bar have?
print(len(coffeebar["CUSTOMER"].unique()))


#Ex 2
#Bar plot 1 of the total amount of sold foods:
foods_count = coffeebar["FOOD"].value_counts()
foods_count.plot.bar(color = "green")

#Bar plot 2 of the total amount of sold drinks:
drinks_count = coffeebar["DRINKS"].value_counts()
drinks_count.plot.bar(color = "green")

#Ex 3: average that a customer buys a certain food or drink at any given time.
#replace the null values "nan" by "nothing", to be used easier.
coffeebar.fillna("nothing", inplace=True)

#create two Serie to separate date and hour
coffeebar[["DATE", "HOUR-TIME"]] = coffeebar['TIME'].str.split(expand=True)

#create a list with all the possible hours in a day, for the coffeebar, from 8:00 to 18:00.
times = coffeebar["HOUR-TIME"].str[0:5].unique()

#Calculate the probabilities in a function.
def probabilities():
    #initialize the dictionaries
    proba_drinks = {}
    proba_foods = {}

    #for each element in the list "times", we create the keys with all the possible hours in a day.
    for time in times:
        proba_drinks[time]= {}
        proba_foods[time]= {}

        #Creation of dictionaries in the first dictionaries: all the possible food and drinks in the coffeebar.
        #Add a key named "TOTAL" to count the total food/drink sold at the current hour.
        #Initialisation of the dictionaries to 0.
        for drink in coffeebar["DRINKS"].unique():
            proba_drinks[time][drink] = 0
        proba_drinks[time]["TOTAL"] = 0

        for food in coffeebar["FOOD"].unique():
            proba_foods[time][food] = 0
        proba_foods[time]["TOTAL"] = 0

    #Observation of each line of coffeebar, and we add 1 to the drink or food purchased.
    for index,hour in enumerate(coffeebar["HOUR-TIME"]):
        proba_foods[hour[0:5]][coffeebar["FOOD"].iloc[index]] += 1
        proba_drinks[hour[0:5]][coffeebar["DRINKS"].iloc[index]] += 1


    #Calculation of the total of drinks/foods sold for each hour.
    for time in times:
        for drink in coffeebar["DRINKS"].unique():
            proba_drinks[time]["TOTAL"] += proba_drinks[time][drink]
        for food in coffeebar["FOOD"].unique():
            proba_foods[time]["TOTAL"] += proba_foods[time][food]

    #Transformation of the values in the dictionaries into probabilities by using the values in "TOTAL".
    for time in times:
        for drink in coffeebar["DRINKS"].unique():
            proba_drinks[time][drink] = (proba_drinks[time][drink]/proba_drinks[time]["TOTAL"])*100
        for food in coffeebar["FOOD"].unique():
            proba_foods[time][food] = (proba_foods[time][food]/proba_foods[time]["TOTAL"])*100
        #Suppression of the key "TOTAL" and its associated value.
        proba_drinks[time].pop("TOTAL")
        proba_foods[time].pop("TOTAL")

    return proba_drinks, proba_foods
