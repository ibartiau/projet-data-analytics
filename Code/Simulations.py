import pandas as pd
from Code.Exploratory import coffeebar, probabilities
from Code.ClassesOfCustomers import OneTimeCustomer, ReturningCustomer
import random

#Get the probabilities.
proba_drinks, proba_foods = probabilities()

#Replace the value "nan" by nothing.
coffeebar.fillna("nothing", inplace=True)

#Function that generete a coffeebar for 5 years.
def generate_coffeebar(nb_returners, prices, budget_regular, budget_hipster, prob_having_tripadisor):

    #Initialize a dictionary to collect data, and create the dataframe at the end.
    coffeebar_simulation= {"TIME": [], "CUSTOMER": [], "DRINKS": [], "FOOD": [], "AMOUNT_PAID": [], "TYPE_CUSTOMER": [], "SUB_TYPE_CUSTOMER": []}

    #Generate customerID for returners (there are 1000 returners) and keep them in a list
    numberid = 10000000
    customerid = "CID" + str(numberid)

    all_returners = []
    for i in range(nb_returners):
        all_returners.append([customerid, "type"])
        numberid += 10
        customerid = "CID" + str(numberid)

    #Create the dictionary where the history will take place and initialize the budget for all returners.
    tracks_returners = {}
    budget_returners = {}
    for i in range(len(all_returners)):
        tracks_returners[all_returners[i][0]] = {}
        if random.randint(1, 100) <= 33:
            sub_type = "hipster"
            all_returners[i][1] = "hipster"
        else:
            sub_type = "regular"
            all_returners[i][1] = "regular"

        object_customer = ReturningCustomer(all_returners[i][0])
        budget_returners[all_returners[i][0]] = object_customer.initBudget(sub_type, budget_regular, budget_hipster)

    #Gather the orders for 5 years.
    for time in coffeebar["TIME"]:
        #Define the type of the customer: Returning or OneTimeCustomer?
        if random.randint(1, 10) <= 2 and len(all_returners) > 0:
            customer_type = "returning"
        else:
            customer_type = "oneTimeCustomer"

        #Create the order for a returning customer.
        if customer_type == "returning":

            #sub_type already defined (hipster or regular)
            #Choose the customerid in the list previously created.
            customer = random.choice(all_returners)
            customerid = customer[0]
            sub_type = customer[1]
            #Make an order (object customer is already defined)
            object_customer = ReturningCustomer(customerid)
            order_list, tracks_returners = object_customer.order(customerid, prices, budget_returners, time, proba_drinks, proba_foods, tracks_returners)

            #Remove the returner from the list of all the returners if he has no more enough money to buy the drink and the food the most expensive.
            if budget_returners[customerid] < 10:
                all_returners.remove([customerid, sub_type])

        #Create the order for a one time customer.
        elif customer_type == "oneTimeCustomer":

            #Create the id for the customer.
            numberid += 10
            customerid = "CID" + str(numberid)

            #Define whether the customer is a tripadvisoror or a regular.
            if random.randint(1, 10) <= prob_having_tripadisor:
                sub_type = "tripadvisor"
            else:
                sub_type = "regular"

            #Create the object customer and make an order.
            object_customer = OneTimeCustomer(customerid, sub_type)
            order_list = object_customer.order(prices, time, proba_drinks, proba_foods)

            #Give a tip to the coffeebar if the customer is a tripadvisor.
            if object_customer.sub_type == "tripadvisor":
                order_list[2] += object_customer.tip(sub_type)

        #Add the data collected in a dictionary of lists.
        coffeebar_simulation["TIME"].append(time)
        coffeebar_simulation["DRINKS"].append(order_list[1])
        coffeebar_simulation["FOOD"].append(order_list[0])
        coffeebar_simulation["CUSTOMER"].append(customerid)
        coffeebar_simulation["AMOUNT_PAID"].append(order_list[2])
        coffeebar_simulation["TYPE_CUSTOMER"].append(customer_type)
        coffeebar_simulation["SUB_TYPE_CUSTOMER"].append(sub_type)

    #create the new dataframe with the information of the dictionary "coffeebar_simulation".
    df_coffeebar_simulation = pd.DataFrame(coffeebar_simulation)
    df_coffeebar_simulation = df_coffeebar_simulation[['TIME', 'CUSTOMER', 'DRINKS', "FOOD", "AMOUNT_PAID", "TYPE_CUSTOMER", "SUB_TYPE_CUSTOMER"]]

    return df_coffeebar_simulation, tracks_returners, all_returners
