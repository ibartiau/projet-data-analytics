import random


class Customer (object):
    def __init__(self, customerid):
        self.customerid = customerid

    def order(self, prices, time, proba_drinks, proba_foods):
        #Form of time: "date + hour". Example: "2013-01-01 08:00:00".

        #Choose which drink or food the customer is going to order, by using the probability previously determined.
        #Create list with 100 elements, corresponding to the calculated percentages.
        all_drinks = []
        all_foods = []

        #Complete the lists by using probabilities.
        #Example: If I have 14 % of chance to buy some soda, I put the element soda 14 times in the list.
        for drink in proba_drinks[time[11:16]]:
            for i in range(int(proba_drinks[time[11:16]][drink])):
                all_drinks.append(drink)
        for food in proba_foods[time[11:16]]:
            for i in range(int(proba_foods[time[11:16]][food])):
                all_foods.append(food)

        #Choose randomly what food or drink the customer takes, by using the lists which we have just created.
        drink = random.choice(all_drinks)
        food = random.choice(all_foods)

        #Remove the prices of the order from the budget of the customer and calculate the amount paid.
        self.budget -= prices[drink]
        self.budget -= prices[food]
        amount_paid = prices[drink] + prices[food]

        #Return all the informations of the order.
        order = [food, drink, amount_paid]
        return order

class OneTimeCustomer (Customer):
    def __init__(self, customerid, sub_type):
        self.customerid = customerid
        self.sub_type = sub_type
        self.budget = 100
        #sub_type = regular or tripadvisor.

    #If sub_type = tripadvisor: give regular tip between 1 and 10, and remove the tip from the budget of the customer.
    def tip(self, sub_type):
        tip = 0
        if sub_type == "tripadvisor":
            tip = random.randint(1,10)
        self.budget -= tip
        return tip


class ReturningCustomer(Customer):
    def __init__(self, customerid):
        self.customerid = customerid

    def initBudget(self, sub_type, budget_regular, budget_hipster):
        #sub_type = regular or hipster
        self.sub_type = sub_type

        if sub_type == "regular":
            budget = budget_regular
        elif sub_type == "hipster":
            budget = budget_hipster
        return budget

    def order(self, customerid, prices, budget_returners, time, proba_drinks, proba_foods, tracks_returners):
        #Form of time: "date + hour". Example: "2013-01-01 08:00:00".

        #Choose which drink or food the customer is going to order, by using the probability previously determined.
        #Create list with 100 elements, corresponding to the calculated percentages.
        all_drinks = []
        all_foods = []

        # Complete the lists by using probabilities.
        # Example: If I have 14 % of chance to buy some soda, I put the element soda 14 times in the list.
        for drink in proba_drinks[time[11:16]]:
            for i in range(int(proba_drinks[time[11:16]][drink])):
                all_drinks.append(drink)
        for food in proba_foods[time[11:16]]:
            for i in range(int(proba_foods[time[11:16]][food])):
                all_foods.append(food)

        #Choose randomly what food or drink the customer takes, by using the lists which we have just created.
        drink = random.choice(all_drinks)
        food = random.choice(all_foods)

        #Remove the prices of the order from the budget of the customer and calculate the amount paid.
        budget_returners[self.customerid] -= prices[drink]
        budget_returners[self.customerid] -= prices[food]
        amount_paid = prices[drink] + prices[food]

        #Return all the informations of the order.
        #Keep track of the order of the returning customer.
        order = [food, drink, amount_paid]
        tracks_returners[self.customerid][time] = [food, drink, budget_returners[self.customerid]]
        return order, tracks_returners